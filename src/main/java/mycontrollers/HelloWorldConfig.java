package mycontrollers;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration //prepares his class as a configuration file
@ComponentScan({"mycontrollers"}) //registers base packages
public class HelloWorldConfig {

	//create a method that returns a well set object of internakResourceViewRessoilver
	@Bean
	public InternalResourceViewResolver internalResourceViewResolver() {
		InternalResourceViewResolver iViewResolver  = new InternalResourceViewResolver();
		iViewResolver.setPrefix("/WEB-INF/views/");
		iViewResolver.setSuffix(".jsp");
		return iViewResolver;
	}
	
	
}
