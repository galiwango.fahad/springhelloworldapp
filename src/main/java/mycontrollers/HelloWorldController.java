package mycontrollers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import jdk.nashorn.internal.ir.RuntimeNode.Request;

@Controller
@RequestMapping(value="/")
public class HelloWorldController {
     
	@RequestMapping(value="/homePage" , method=RequestMethod.GET)
	public String homePage() {
	
		return "index";
	}
	
	@RequestMapping(value="/welcomepage")
	public String goToIndex() {
	
		return "welcomePage";
	}
	@RequestMapping(value="/add")
	//ModelAndView add(HttpServletRequest request , HttpServletResponse response)
	//using @Requestparam annotation to bind values
	public ModelAndView add(@RequestParam("num1") int n1 , @RequestParam("num2") int n2 , HttpServletResponse response) {
		
//		int n1 = Integer.parseInt(request.getParameter("num1"));
//		int n2 = Integer.parseInt(request.getParameter("num2"));
	    int k = n1+n2;
	    
	    ModelAndView mAndView = new ModelAndView();
	    mAndView.setViewName("welcomePage");
	    mAndView.addObject("result", k);
	    
	    return mAndView;
	}
}
